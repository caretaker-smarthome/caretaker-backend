<?php

use Defuse\Crypto\Crypto;

class MyCrypto
{

    protected $myCryptoSettings;

    public function __construct(array $settings)
    {
        $this->myCryptoSettings = $settings;
    }

    protected function getEncryptionKey()
    {
        // TODO: Use json
        $myfile = fopen($this->myCryptoSettings["key_to_encrypt_path"], "r");
        $pwd = fread($myfile, 100);
        fclose($myfile);

        return $pwd;
    }

    public function encrypt(string $msg)
    {
        return Crypto::encryptWithPassword($msg, self::getEncryptionKey());
    }

    public function decrypt(string $msg)
    {
        try {
            return Crypto::decryptWithPassword($msg, self::getEncryptionKey());
        } catch (Exception $e) {
            return '';
        }
    }

    public function hashPassword(string $pwd)
    {
        return password_hash($pwd, PASSWORD_DEFAULT);
    }

    public function verifyPassword(string $pwd, string $hashedPwd)
    {
        return password_verify($pwd, $hashedPwd);
    }
}
