<?php


// IMPORT HELPER ZONE
require_once dirname(__FILE__) . "/helper.php";
// END IMPORT HELPER ZONE


class ApiSettings
{
    protected $PATH_TO_SETTINGS;
    protected $API_SETTINGS;

    public function __construct()
    {
        $this->PATH_TO_SETTINGS = dirname(__FILE__) . '/api-settings.json';
        self::setApiSettings();
    }

    protected function setApiSettings()
    {
        $this->API_SETTINGS = Helper::getJsonFromFile($this->PATH_TO_SETTINGS);
    }

    public function getPathToApi()
    {
        $apiToUse = $this->API_SETTINGS["api_to_use"];
        return $this->API_SETTINGS["api"][$apiToUse];
    }

    public function getPathToWs()
    {
        $wsToUse = $this->API_SETTINGS["ws_to_use"];
        return $this->API_SETTINGS["ws"][$wsToUse];
    }

    public function getCryptoSettings()
    {
        return $this->API_SETTINGS["crypto"];
    }

    public function getGmailSettings()
    {
        return $this->API_SETTINGS["gmail"];
    }

    public function getLogsHandlerSettings()
    {
        return $this->API_SETTINGS["logs_handler"];
    }

    public function getDatabaseSettings()
    {
        return $this->API_SETTINGS["database"];
    }

    public function getDevsEmail()
    {
        return $this->API_SETTINGS["devs_email"];
    }
}
