<?php

require_once dirname(__FILE__) . "/../vendor/autoload.php";

// IMPORT RATCHET LIBRARIE
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
// END IMPORT RATCHET LIBRARIE

require_once dirname(__FILE__) . "/helper.php";
require_once dirname(__FILE__) . "/apiSettings.php";

// IMPORT LOGS HANDLER ZONE
require_once dirname(__FILE__) . "/logsHandler.php";
// END IMPORT LOGS HANDLER ZONE

require_once dirname(__FILE__) . "/emailSender.php";

interface IWebsocket
{
    public function _onOpen(Ratchet\ConnectionInterface $conn): void;
    public function _onClose(Ratchet\ConnectionInterface $conn): void;
    public function _onMessage(Ratchet\ConnectionInterface $from, array $msg): void;
}

class Websocket implements MessageComponentInterface
{
    public $connections;
    public $route;

    protected $apiSettings;
    protected $logsHandler;
    protected $emailSender;

    public function __construct()
    {
        $this->apiSettings = new ApiSettings();

        $this->connections = new \SplObjectStorage;

        $this->emailSender = new EmailSender($this->apiSettings->getGmailSettings(), function (string $errorMsg) {
            $this->onSendEmailError($errorMsg);
        });

        $logsHandlerSettings = array_merge(
            $this->apiSettings->getLogsHandlerSettings(),
            array("devs_email" => $this->apiSettings->getDevsEmail())
        );
        $this->logsHandler = new LogsHandler($logsHandlerSettings, $this->emailSender);
    }

    public function onOpen(Ratchet\ConnectionInterface $conn)
    {
        $this->connections->attach($conn);
        $this->_onOpen($conn);

        echo "\n>>> New connection [{$conn->resourceId}]";
    }

    public function onMessage(Ratchet\ConnectionInterface $from, $msg)
    {
        if (Helper::isJson($msg)) {
            $data = json_decode($msg, TRUE);
            $this->_onMessage($from, $data);
        } else {
            $from->send("The incoming data are invalid -> '" . htmlspecialchars($msg) . "'");
        }
    }

    public function onClose(Ratchet\ConnectionInterface $conn)
    {
        $this->connections->detach($conn);
        $this->_onClose($conn);

        echo "\n>>> Connection closed [{$conn->resourceId}]";
    }

    public function onError(Ratchet\ConnectionInterface $conn, \Exception $e)
    {
        $this->logsHandler->addLog("alert", "Error has occurred in a Websocket", array(
            "error" => $e->getMessage(),
        ));

        $conn->close();
    }

    public function addLog(string $logType, string $title, array $additionalData = [])
    {
        $this->logsHandler->addLog($logType, $title, $additionalData);
    }

    public function run(object $class, string $route, $port = 80): void
    {
        $this->route = $route;

        $app = new Ratchet\App('localhost', $port);
        $app->route($this->apiSettings->getPathToWs() . $route, $class, array('*'));
        $app->run();
    }
}
