<?php

require_once "../../request.php";

class CreateUserAwaitingConfirmation extends Request implements IRequest
{

	protected $cmdName = "CreateUserAwaitingConfirmation";

	public function request()
	{

		$this->getRequestData();

		try {
			$this->adventsKalenderDB->connect();

			$isMailPresentInUser_to_check = $this->adventsKalenderDB->isPresent("SELECT * FROM user_to_check WHERE email= :email", array(
				"email" => $this->userBody["email"],
			));

			$isMailPresentInUser = $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE email= :email", array(
				"email" => $this->userBody["email"],
			));

			if (!$isMailPresentInUser_to_check && !$isMailPresentInUser) {

				$userAwaitingConfirmationId = Helper::getId();

				$emailConstructor = array(
					"email"			  =>  array($this->userBody["email"]),
					"subject"		  => "Account creation : ISFATES Advents Kalender",
					"bodyConstructor" => array(
						"pathToTemplate" => "./validEmailTemplate.html",
						"replacer"		 => array(
							"__PATH_TO_API__"      => $this->apiSettings->getPathToApi(),
							"__USER_TO_VALID_ID__" => $userAwaitingConfirmationId,
						),
					)
				);

				$this->emailSender->sendEmail($emailConstructor);

				if ($this->emailSender->hasEmailBeenSend) {

					$this->insertUserToCheck(
						$userAwaitingConfirmationId,
						$this->userBody["email"],
						$this->userBody["password"]
					);

					$this->returnSuccessResponse(
						array(
							"hasUserBeenCreated" => TRUE,
						)
					);
				} else {
					$this->returnErrorResponse("Impossible to send Email !");
				}
			} else {
				$this->returnSuccessResponse(
					array(
						"hasUserBeenCreated" => FALSE,
					)
				);
			}

			$this->adventsKalenderDB->disconnect();
		} catch (Exception $e) {
			$this->addLog("debug", "Critical error", array(
				"cmd"   => $this->cmdName,
				"error" => $e->getMessage(),
			));

			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function insertUserToCheck(string $userId, string $email, string $pwd)
	{
		$this->adventsKalenderDB->query(
			"INSERT INTO user_to_check(id, email, name, first_name, phone, pwd) VALUES(:id, :email, :name, :first_name, :phone, :pwd)",
			array(
				"id"    => $userId,
				"email" => $email,
				"name" => $this->userBody["name"],
				"first_name" => $this->userBody["first_name"],
				"phone" => $this->userBody["phone"],
				"pwd"   => $this->myCrypto->hashPassword($pwd),
			)
		);
	}
}

$createUserAwaitingConfirmation = new CreateUserAwaitingConfirmation();
$createUserAwaitingConfirmation->request();
