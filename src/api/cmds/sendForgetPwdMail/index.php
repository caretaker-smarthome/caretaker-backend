<?php

require_once "../../request.php";

class SendForgetPwdMail extends Request implements IRequest
{
    protected $cmdName = "SendForgetPwdMail";

    public function request()
    {

        $this->getRequestData();

        try {
            $this->adventsKalenderDB->connect();

            $doesEmailExist = $this->doesEmailExistInDB($this->userBody["email"]);

            if ($doesEmailExist) {
                $this->sendForgetPwdMailRequest($this->userBody["email"]);
            } else {
                $this->returnSuccessResponse(
                    array(
                        "hasEmailBeSend" => FALSE,
                        "msg"            => "Unknown email", // TODO: Handle error with number ?
                    )
                );
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function sendForgetPwdMailRequest(string $email)
    {
        $resetPwdToken = Helper::getId();

        $userId = $this->getUserIdFromEmail($email);

        if (!$this->doesUserIdAlreadyExist($userId)) {
            $this->insertResetPwdTokenInDb($resetPwdToken, $userId);

            $emailConstructor = array(
                "email"           => array($email),
                "subject"         => "Reset my password : ISFATES Adventskalender",
                "bodyConstructor" => array(
                    "pathToTemplate" => "./forgetPwdMailTemplate.html",
                    "replacer"       => array(
                        "__PATH_TO_API__" => $this->apiSettings->getPathToApi(),
                        "__RESET_PWD_TOKEN__" => urlencode($resetPwdToken),
                    ),
                )
            );

            $this->emailSender->sendEmail($emailConstructor);

            if ($this->emailSender->hasEmailBeenSend) {
                $this->returnSuccessResponse(
                    array(
                        "hasEmailBeSend" => TRUE,
                    )
                );
            } else {
                $this->returnErrorResponse("Impossible to send Email !");
            }
        } else {
            $this->returnSuccessResponse(
                array(
                    "hasEmailBeSend" => FALSE,
                    "msg"            => "You have already requested a password change. Please check your emails!", // TODO: Handle error with number ?
                )
            );
        }
    }

    protected function insertResetPwdTokenInDb(string $resetPwdToken, string $userId)
    {
        $this->adventsKalenderDB->query(
            "INSERT INTO reset_pwd_token(resetPwdToken, userId) VALUES(:resetPwdToken, :userId)",
            array(
                "resetPwdToken" => $resetPwdToken,
                "userId"        => $userId,
            )
        );
    }

    protected function doesEmailExistInDB(string $email)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE email= :email", array(
            "email" => $email,
        ));
    }

    protected function doesUserIdAlreadyExist(string $userId)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM reset_pwd_token WHERE userId= :userId", array(
            "userId" => $userId,
        ));
    }

    protected function getUserIdFromEmail(string $email): string
    {
        $getUserIdResponse = $this->adventsKalenderDB->get("SELECT id FROM user WHERE email= :email", array(
            "email" => $email,
        ));

        return $getUserIdResponse[0]["id"];
    }
}

$sendForgetPwdMail = new SendForgetPwdMail();
$sendForgetPwdMail->request();
