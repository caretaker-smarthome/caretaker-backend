<?php

require_once "../../request.php";

class CreateUser extends Request implements IRequest
{
	protected $cmdName = "CreateUser";
	protected $userIdToCreate;
	public $hasUserBeenCreated;


	protected function getPseudoFromEmail(string $email)
	{
		$explodedEmail = explode("@", $email);

		return $explodedEmail[0];
	}

	public function request()
	{

		try {
			$this->userIdToCreate = $_GET["id"];

			$this->adventsKalenderDB->connect();

			if ($this->adventsKalenderDB->isPresent("SELECT * FROM user_to_check WHERE id= :userIdToCreate", array(
				"userIdToCreate" => $this->userIdToCreate,
			))) {

				$userData = $this->adventsKalenderDB->get("SELECT * FROM user_to_check WHERE id= :userIdToCreate", array(
					"userIdToCreate" => $this->userIdToCreate,
				));

				$userData = $userData[0];

				$userId = $this->generateUserId();

				$this->adventsKalenderDB->query(
					"INSERT INTO user(id, pseudo, email, name, first_name, phone, pwd, lang) VALUES(:id, :pseudo, :email, :name, :first_name, :phone, :pwd, :lang)",
					array(
						"id"    	  => $userId,
						"pseudo" 	  => $this->getPseudoFromEmail($userData["email"]),
						"email" 	  => $userData["email"],
						"name"		  => $userData["name"],
						"first_name"  => $userData["first_name"],
						"phone"       => $userData["phone"],
						"pwd"   	  => $userData["pwd"],
						"lang" 		  => "en",
					)
				);

				$this->adventsKalenderDB->query(
					"DELETE FROM user_to_check WHERE id=:userIdToCreate",
					array(
						"userIdToCreate" => $this->userIdToCreate,
					)
				);

				$this->logUserCreation($userId);
				$this->hasUserBeenCreated = TRUE;
			} else {
				$this->hasUserBeenCreated = FALSE;
			}

			$this->adventsKalenderDB->disconnect();
		} catch (Exception $e) {
			$this->addLog("critical", "Critical error", array(
				"cmd"   => $this->cmdName,
				"error" => $e->getMessage(),
			));

			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function logUserCreation(string $userId)
	{
		$this->addLog("info", "User creation", array(
			"cmd"    => $this->cmdName,
			"userId" => $userId,
		));
	}

	protected function generateUserId()
	{
		$generatedId = Helper::getId();

		if ($this->doesUserIdAlreadyExist($generatedId)) {
			return $this->generateUserId();
		}

		return $generatedId;
	}

	protected function doesUserIdAlreadyExist(string $userId)
	{
		return $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE id= :idToCheck", array(
			"idToCheck" => $userId,
		));
	}
}

$createUser = new CreateUser();
$createUser->request();

$data  = array(
	TRUE  =>  array(
		"class" => "success",
		"msg"   => "Your account has been successfully created",
	),
	FALSE =>  array(
		"class" => "danger",
		"msg"   => "Error", // TODO: Find better msg
	),
);

$dataToUse = $data[$createUser->hasUserBeenCreated];

// TODO: Show nice validation page
// TODO: Store validation page in external static .html file
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">

	<title>AdventsKalender - Create user validation</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

</head>

<body>


	<div class="alert alert-<?php echo $dataToUse["class"]; ?>" role="alert">
		<h1><?php echo $dataToUse["msg"]; ?></h1>
	</div>



	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</body>

</html>