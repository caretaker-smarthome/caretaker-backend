<?php

require_once "../../request.php";


class IsUser extends Request implements IRequest
{

    protected $cmdName = "IsUser";
    protected $userId;
    protected $userTokens;
    protected $MAX_TOKENS_NUMBER = 5;

    public function request()
    {
        $this->getRequestData();

        try {
            $this->adventsKalenderDB->connect();

            $getUserData = $this->adventsKalenderDB->get("SELECT id, pwd FROM user WHERE email= :email", array(
                "email" => $this->userBody["email"],
            ));

            if ($getUserData !== [] && $this->myCrypto->verifyPassword($this->userBody["password"], $getUserData[0]["pwd"])) { // TODO: Refactoring
                $this->userId = $getUserData[0]["id"];
                $generatedToken = $this->getGeneratedToken();
                $this->userTokens = $this->getUserTokens();

                if (sizeof($this->userTokens) >= $this->MAX_TOKENS_NUMBER) {
                    $this->replaceOldestToken($generatedToken);
                } else {
                    $this->createTokenAssociationInDb($generatedToken);
                }

                $this->returnSuccessResponse(array(
                    "isUser" => TRUE,
                    "token"  => $this->myCrypto->encrypt($generatedToken),
                ));
            } else {
                $this->returnSuccessResponse(array("isUser" => FALSE));
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function getGeneratedToken()
    {
        $generatedToken = "token-" . Helper::getId();

        if ($this->doesTokenAlreadyExist($generatedToken)) {
            return $this->getGeneratedToken();
        }

        return $generatedToken;
    }

    protected function doesTokenAlreadyExist(string $token)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM token WHERE id= :idToCheck", array(
            "idToCheck" => $token,
        ));
    }

    protected function createTokenAssociationInDb(string $tokenId)
    {
        $this->adventsKalenderDB->query(
            "INSERT INTO token(id, user_id) VALUES(:id, :user_id)",
            array(
                "id"     => $tokenId,
                "user_id" => $this->userId,
            )
        );
    }

    protected function replaceTokenAssociationInDb(string $tokenIdToReplace, string $tokenId)
    {
        $this->adventsKalenderDB->query(
            "UPDATE token SET id = :newId WHERE id = :id AND user_id = :userId",
            array(
                "id"      => $tokenIdToReplace,
                "newId"   => $tokenId,
                "userId"  => $this->userId,
            )
        );
    }

    protected function getUserTokens()
    {
        $userTokensResponse = $this->adventsKalenderDB->get("SELECT id, creation_date FROM token WHERE user_id= :userId ORDER BY creation_date ASC", array(
            "userId" => $this->userId,
        ));

        return $userTokensResponse;
    }

    protected function replaceOldestToken(string $tokenId)
    {
        $oldestToken = $this->userTokens[0];
        $this->replaceTokenAssociationInDb($oldestToken["id"], $tokenId);
    }
}

$isUserClass =  new IsUser();
$isUserClass->request();
