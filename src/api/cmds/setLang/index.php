<?php

require_once "../../request.php";

class SetLang extends Request implements IRequest
{

    protected $cmdName = "SetLang";
    protected $userIdToCreate;

    public function request()
    {

        try {
            $this->getRequestData();

            $this->adventsKalenderDB->connect();

            $cryptedToken = $this->userBody["token"];

            if ($this->isTokenValid($cryptedToken)) {
                $this->setLangRequest($this->getDecryptedToken($cryptedToken), $this->userBody["lang"]);
            } else {
                $this->returnErrorResponse("Invalid Token");
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function setLangRequest(string $token, string $lang)
    {
        $getUserIdResponse = $this->adventsKalenderDB->get("SELECT user_id FROM token WHERE id= :id", array(
            "id" => $token,
        ));

        $userId = $getUserIdResponse[0]["user_id"];

        $this->adventsKalenderDB->query(
            "UPDATE user SET lang = :lang WHERE id = :id",
            array(
                "id"   => $userId,
                "lang" => $lang,
            )
        );

        $this->logSetLang($userId, $lang);

        $this->returnSuccessResponse(
            array(
                "hasLangBeChanged" => TRUE,
            )
        );
    }

    protected function logSetLang(string $userId, string $lang)
    {
        $this->addLog("info", "User change lang", array(
            "cmd"    => $this->cmdName,
            "userId" => $userId,
            "lang"   => $lang,
        ));
    }
}


$setLang = new SetLang();
$setLang->request();
