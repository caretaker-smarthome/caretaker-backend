<?php

require_once "../../request.php";

class GetUserData extends Request implements IRequest
{
    protected $cmdName = "GetUserData";
    protected $userIdToCreate;

    public function request()
    {

        try {
            $this->getRequestData();

            $this->adventsKalenderDB->connect();

            if ($this->isTokenValid($this->userBody["token"])) {
                $cryptedToken = $this->userBody["token"];
                $token = $this->getDecryptedToken($cryptedToken);

                $getUserIdResponse = $this->adventsKalenderDB->get("SELECT user_id FROM token WHERE id= :id", array(
                    "id" => $token,
                ));

                $userId = $getUserIdResponse[0]["user_id"];

                $userData = $this->adventsKalenderDB->get("SELECT pseudo, email, lang FROM user WHERE id= :id", array(
                    "id" => $userId,
                ));

                $this->logGetUserData($userId);

                $this->returnSuccessResponse(
                    array(
                        "pseudo"       => $userData[0]["pseudo"],
                        "email"        => $userData[0]["email"],
                        "lang"         => $userData[0]["lang"],
                        "avatarLink"   => $this->getAvatarLink(),
                    )
                );
            } else {
                $this->addLog("notice", "Try to connect with invalid token", array(
                    "cmd"        => $this->cmdName,
                    "givenToken" => $this->userBody["token"],
                ));

                $this->returnErrorResponse("Invalid token");
            }


            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function logGetUserData(string $userId)
    {
        $this->addLog("info", "User get data", array(
            "cmd"    => $this->cmdName,
            "userId" => $userId,
        ));
    }

    protected function getUnlockedDays()
    {
        // TODO: Remove comment from this lines
        /*
        if (date("n") !== "12") { // if month is not december
            return array(); // no days are unlocked
        }
        */

        return range(1, 20);
    }

    protected function getAvatarLink()
    {
        // TODO: Finish getAvatarLink method
        return "https://johannchopin.fr/img/adventskalender/gift_000_img01.png";
    }
}

$getUserDataClass = new GetUserData();
$getUserDataClass->request();
