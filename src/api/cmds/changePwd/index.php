<?php

require_once "../../request.php";


class ChangePwd extends Request implements IRequest
{

    protected $cmdName = "ChangePwd";

    public function request()
    {
        try {
            $this->getRequestData();

            $cryptedToken = $this->userBody["token"];

            $this->adventsKalenderDB->connect();

            if ($this->isTokenValid($cryptedToken)) {

                $userId = $this->getUserId($cryptedToken);

                $getUserPwdResponse = $this->adventsKalenderDB->get("SELECT pwd FROM user WHERE id= :id", array(
                    "id" => $userId,
                ));

                $userPwd = $getUserPwdResponse[0]["pwd"];

                if ($this->myCrypto->verifyPassword($this->userBody["pwd"], $userPwd)) {
                    $this->changeUserPwdInDb($userId, $this->userBody["newPwd"]);
                    $this->deleteUserTokenInDb($userId);
                    $this->adventsKalenderDB->disconnect();

                    $this->returnSuccessResponse(array(
                        "hasPwdBeChanged" => TRUE,
                    ));
                }
            }

            $this->adventsKalenderDB->disconnect();
            $this->returnSuccessResponse(array("hasPwdBeChanged" => FALSE));
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function changeUserPwdInDb(string $userId, string $newPwd)
    {
        $this->adventsKalenderDB->query(
            "UPDATE user SET pwd = :newPwd WHERE id = :userId",
            array(
                "userId" => $userId,
                "newPwd" => $this->myCrypto->hashPassword($newPwd),
            )
        );
    }

    protected function deleteUserTokenInDb(string $userId)
    {
        $this->adventsKalenderDB->query(
            "DELETE FROM token WHERE user_id= :userIdToDelete",
            array(
                "userIdToDelete" => $userId,
            )
        );
    }
}

$changePwdClass = new ChangePwd();
$changePwdClass->request();
