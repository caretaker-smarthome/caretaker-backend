<?php

require_once "../../request.php";

class ResetPassword extends Request implements IRequest
{

    protected $cmdName = "ResetPassword";
    protected $tokenToResetPwd;
    protected $hasEmailBeenSend = FALSE;

    public function request()
    {
        try {
            $this->tokenToResetPwd = $_GET["t"];

            $this->adventsKalenderDB->connect();

            if ($this->adventsKalenderDB->isPresent("SELECT * FROM reset_pwd_token WHERE resetPwdToken= :resetPwdToken", array(
                "resetPwdToken" => $this->tokenToResetPwd,
            ))) {

                $userIdResponse = $this->adventsKalenderDB->get("SELECT userId FROM reset_pwd_token WHERE resetPwdToken= :resetPwdToken", array(
                    "resetPwdToken" => $this->tokenToResetPwd,
                ));

                $userId = $userIdResponse[0]["userId"];
                $userEmail = $this->getUserEmail($userId);

                $generatedPwd = $this->getGeneratedPwd();
                $cryptedGeneratedPwd = $this->myCrypto->hashPassword(md5($generatedPwd));

                $this->sendMailWithNewPwd($userEmail, $generatedPwd);

                if ($this->hasEmailBeenSend) {
                    $this->updateUserPwd($userId, $cryptedGeneratedPwd);
                    $this->deleteResetPwdInDb();

                    $this->logResetPwd($userId);

                    Helper::redirectToLink("success.html");
                } else {
                    $this->addLog("critical", "Impossible to send email", array(
                        "cmd"   => $this->cmdName,
                        "email" => $userEmail,
                    ));
                }
            } else {
                // TODO: Fix bug -> why does it come here ?
                $this->addLog("warning", "Reset pwd without valid \"resetPwdToken\"", array(
                    "cmd"           => $this->cmdName,
                    "resetPwdToken" => $this->tokenToResetPwd,
                ));

                Helper::redirectToLink("error.html");
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function logResetPwd(string $userId)
    {
        $this->addLog("info", "Reset pwd has succeeded", array(
            "cmd"    => $this->cmdName,
            "userId" => $userId,
        ));
    }

    protected function deleteResetPwdInDb()
    {
        $this->adventsKalenderDB->query(
            "DELETE FROM reset_pwd_token WHERE resetPwdToken = :resetPwdToken",
            array(
                "resetPwdToken" => $this->tokenToResetPwd,
            )
        );
    }

    protected function updateUserPwd(string $userId, string $generatedPwd)
    {
        $this->adventsKalenderDB->query(
            "UPDATE user SET pwd = :pwd WHERE id = :userId",
            array(
                "pwd"    => $generatedPwd,
                "userId" => $userId,
            )
        );
    }

    protected function getGeneratedPwd(): string
    {
        return strval(rand(10000, 99999));
    }

    protected function getUserEmail(string $userId): string
    {

        $userEmailResponse = $this->adventsKalenderDB->get("SELECT email FROM user WHERE id = :userId", array(
            "userId" => $userId,
        ));

        return $userEmailResponse[0]["email"];
    }

    protected function sendMailWithNewPwd(string $email, string $newPwd)
    {
        $emailConstructor = array(
            "email"           => array($email),
            "subject"         => "New password : ISFATES Advents Kalender",
            "bodyConstructor" => array(
                "pathToTemplate" => "./mailWithNewPwdTemplate.html",
                "replacer"       => array(
                    "__NEW_PWD__" => urlencode($newPwd),
                ),
            )
        );

        $this->emailSender->sendEmail($emailConstructor);

        if ($this->emailSender->hasEmailBeenSend) {
            $this->hasEmailBeenSend = TRUE;
        }
    }
}

$resetPwdClass = new ResetPassword();
$resetPwdClass->request();
