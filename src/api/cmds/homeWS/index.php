<?php

require_once dirname(__DIR__) . "../../websocket.php";

class HomeWS extends Websocket implements IWebsocket
{
    protected $ACTIONS_CODE = array(
        "identification"             => 1, // On new connexion, client needs to identify himself
        "identificationHasSucceeded" => 2,
    );

    // List of all client id ($conn->resourceId)
    // that have to send a message of type "identification"
    protected $clientsIdToIdentify = array();
    protected $clients = array();
    protected $homeConnexionId = NULL;

    protected function getActionCode(string $actionName)
    {
        if (array_key_exists($actionName, $this->ACTIONS_CODE)) {
            return $this->ACTIONS_CODE[$actionName];
        } else {
            $this->addLog("warning", "Invalid given action name", array(
                "givenActionName" => $actionName,
            ));

            // TODO: Send message to client and close connexion
        }
    }

    protected function getActionName(int $actionCode): string
    {
        foreach ($this->ACTIONS_CODE as $name => $code) {
            if ($code === $actionCode) {
                return $name;
            }
        }
    }

    protected function addToClientToIdentify(Ratchet\ConnectionInterface $conn): void
    {
        $connId = $conn->resourceId;

        if (!array_key_exists($connId, $this->clientsIdToIdentify)) {
            $this->clientsIdToIdentify[$connId] = array();
        } else {
            // TODO: If key exist add a log
            echo ("clientsIdToIdentify allready exist");
        }
    }

    protected function isClientAHome(array $msg): bool
    {
        return $msg["header"]["sender_type"] === "home";
    }

    protected function identifyClient(Ratchet\ConnectionInterface $conn, array $msg): void
    {
        $connId = $conn->resourceId;
        $msgBody = $msg["body"];
        $msgHeader = $msg["header"];

        if ($this->isClientAHome($msg) && ($this->homeConnexionId === NULL)) {
            echo ("\n>>> Set homeConnexionId to [{$connId}]");
            $this->homeConnexionId = $connId;
        }

        if (!array_key_exists($connId, $this->clients)) {
            $this->clients[$connId] = array(
                "type" => $msgHeader["sender_type"],
            );
        } else {
            // TODO: If key exist add a log
        }

        $infos = $this->getHomeWsInfos();
        $infos["body"]["actions"] = array(
            array(
                "name" => "identification-success",
                "id"   => $this->getActionCode("identificationHasSucceeded"),
            ),
        );

        $conn->send(json_encode($infos));

        unset($this->clientsIdToIdentify[$conn->resourceId]);
    }

    protected function sendMessageToHome(array $msg): void
    {
        foreach ($this->connections as $conn) {
            if ($conn->resourceId === $this->homeConnexionId) {
                $conn->send(json_encode($msg));
            }
        }
    }

    protected function sendActionsToHome(array $actions): void
    {
        $request = $this->getHomeWsInfos();

        $request["body"]["actions"] = $actions;

        $this->sendMessageToHome($request);
    }

    protected function handleMessage(Ratchet\ConnectionInterface $conn, array $msg): void
    {
        if (isset($msg["body"]["actions"])) {
            foreach ($msg["body"]["actions"] as $action) {
                $actionTo = $action["to"];

                if ($actionTo === "home" && ($this->homeConnexionId !== NULL)) {
                    $this->sendActionsToHome($msg["body"]["actions"]);
                    echo ("\nMessage send to home " . $this->homeConnexionId);
                }
            }
        }
    }

    protected function isClientIdentified(Ratchet\ConnectionInterface $conn): bool
    {
        return !array_key_exists($conn->resourceId, $this->clientsIdToIdentify);
    }

    public function _onOpen(Ratchet\ConnectionInterface $conn): void
    {
        $this->addToClientToIdentify($conn);
        $this->requestIdentifation($conn);
    }

    public function _onMessage(Ratchet\ConnectionInterface $from, array $msg): void
    {
        if ($this->isClientIdentified($from)) {
            $this->handleMessage($from, $msg);
        } else {
            $msgType = $msg["body"]["actions"][0]["id"]; // TODO: Refactoring...

            if ($this->getActionName($msgType) === "identification") {
                $this->identifyClient($from, $msg);
            }
        }
    }

    public function _onClose(Ratchet\ConnectionInterface $conn): void
    {
        $connId = $conn->resourceId;

        if ($this->homeConnexionId === $connId) {
            $this->homeConnexionId = NULL;
        }

        unset($this->clients[$connId]);
    }

    protected function getHomeWsInfos()
    {
        return array(
            "header" => array(
                "from" => $this->route,
                "sender_type" => "server",
            ),
            "body" => array(),
        );
    }

    protected function requestIdentifation(Ratchet\ConnectionInterface $conn)
    {
        $infos = $this->getHomeWsInfos();
        $infos["body"]["actions"] = array(
            array(
                "name" => "identification",
                "id"   => $this->getActionCode("identification"),
            ),
        );

        $conn->send(json_encode($infos));
    }
}

$homeWS = new HomeWS();
$homeWS->run($homeWS, "homews");
