<?php

require_once "../../request.php";

class IsTokenValid extends Request implements IRequest
{

	protected $cmdName = "IsTokenValid";
	protected $userIdToCreate;

	public function request()
	{

		try {
			$this->getRequestData();

			$this->adventsKalenderDB->connect();

			$cryptedToken = $this->userBody["token"];

			if ($this->isTokenValid($cryptedToken)) {
				$this->addLog("info", "user connect successfully", array(
					"cmdName"   => $this->cmdName,
					"userToken" => $this->getDecryptedToken($cryptedToken),
				));

				$this->returnSuccessResponse(
					array(
						"isTokenValid" => TRUE,
					)
				);
			} else {
				$this->addLog("notice", "user try to connect with invalid token", array(
					"cmd"       => $this->cmdName,
					"userToken" => $this->getDecryptedToken($cryptedToken),
				));

				$this->returnSuccessResponse(
					array(
						"isTokenValid" => FALSE,
					)
				);
			}

			$this->adventsKalenderDB->disconnect();
		} catch (Exception $e) {
			$this->addLog("critical", "Critical error", array(
				"cmd"   => $this->cmdName,
				"error" => $e->getMessage(),
			));

			$this->returnErrorResponse($e->getMessage());
		}
	}
}

$isTokenValisClass = new IsTokenValid();
$isTokenValisClass->request();
