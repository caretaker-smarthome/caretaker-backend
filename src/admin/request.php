<?php

require_once dirname(__FILE__) . "/emailSender.php";
require_once dirname(__FILE__) . "/databaseHandler.php";
require_once dirname(__FILE__) . "/myCrypto.php";

require_once dirname(__FILE__) . "/pathToDefaultsClasses.php";


interface IRequest
{
    public function request();
}


class Request
{


    protected $PATH_TO_SETTINGS;
    protected $API_SETTINGS;
    protected $userHeader;
    protected $userBody;
    protected $myCrypto;
    protected $adventsKalenderDB;
    protected $emailSender;

    function __construct()
    {
        $this->PATH_TO_SETTINGS = dirname(__FILE__) . '/api-settings.json';
        $this->setApiSettings();
        $this->myCrypto = new MyCrypto($this->API_SETTINGS["crypto"]);
        $this->adventsKalenderDB = new DatabaseHandler($this->API_SETTINGS["database"]["db_credentials_path"]);
        $this->emailSender = new EmailSender($this->API_SETTINGS["gmail"], function (string $errorMsg) {
            $this->onSendEmailError($errorMsg);
        });
    }

    protected function getRequestData()
    {
        if ($this->checkRequestExistence()) {
            $this->userHeader = $_POST["header"];
            $this->userBody = $_POST["request"];
        }
    }

    public function checkRequestExistence()
    {
        if (!isset($_POST["header"]["private_key"]) || !$this->isPrivateKeyValid($_POST["header"]["private_key"])) {
            $this->returnErrorResponse("Invalid private key !");
            $this->addLog("warning", "Invalid private key !");
        }

        if (!isset($_POST["request"])) {
            $this->returnErrorResponse("Request have to contain a request !");
            $this->addLog("warning", "Invalid request with invalid request");
        }

        if (!isset($_POST["header"])) {
            $this->returnErrorResponse("Request have to contain a header !");
            $this->addLog("warning", "Invalid request with invalid header");
        }

        return TRUE;
    }

    protected function isPrivateKeyValid(string $privateKey)
    {
        return $privateKey === "91836d48-70150c9f-r8b3a20a-1d752ea9"; // TODO: Don't save it in code !
    }

    public function returnErrorResponse(String $msg)
    {
        $this->returnJson(
            array(
                "success" => FALSE,
                "msg" => $msg,
            )
        );

        die();
    }

    public function returnSuccessResponse(array $response)
    {
        $this->returnJson(
            array(
                "success" => TRUE,
                "response" => $response,
            )
        );

        die();
    }

    protected function returnJson(array $array)
    {
        echo (json_encode($array));
    }

    public function getDecryptedToken(string $cryptedToken)
    {
        return $this->myCrypto->decrypt($cryptedToken);
    }

    protected function setApiSettings()
    {
        $this->API_SETTINGS = Helper::getJsonFromFile($this->PATH_TO_SETTINGS);
    }


    // ###### ERROR HANDLING ZONE
    protected function onSendEmailError(string $errorMsg)
    { }
    // ###### END ERROR HANDLING ZONE


    // ###### SPECIFIC METHODS ZONE
    protected function isTokenValid(string $cryptedToken)
    {
        $token = $this->getDecryptedToken($cryptedToken);

        if ($this->adventsKalenderDB->isPresent("SELECT * FROM token WHERE id= :id", array(
            "id" => $token,
        ))) {
            return TRUE;
        }

        return FALSE;
    }

    public function getUserId(string $cryptedToken): string
    {
        $token = $this->getDecryptedToken($cryptedToken);

        $getUserIdResponse = $this->adventsKalenderDB->get("SELECT user_id FROM token WHERE id= :id", array(
            "id" => $token,
        ));

        return $getUserIdResponse[0][user_id];
    }

    public function getPythonAPICmdResponse(string $cmd, array $args = array())
    {
        $formatedArgs = json_encode($args);

        if ($this->isPythonAPICmdValid($cmd)) {
            $pathToPythonScripts = dirname(__FILE__) . "/driveApi/";

            $pythonScriptResponse = shell_exec("python " . $pathToPythonScripts . "cmd_" . $cmd . ".py " . str_replace('"', '\"', $formatedArgs));

            return json_decode($pythonScriptResponse, TRUE);
        } else {
            $this->returnErrorResponse("Try to run invalid python script !");
        }
    }

    protected function isPythonAPICmdValid(string $cmdName): bool
    {
        $validCmds = array(
            "getBeneficiariesData",
            "getGiftsName",
            "getGivenGiftsData"
        );

        return in_array($cmdName, $validCmds);
    }
    // ###### END SPECIFIC METHODS ZONE

}
