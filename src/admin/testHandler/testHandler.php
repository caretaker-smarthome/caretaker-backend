<?php

require_once dirname(__FILE__) . "/testHandlerHelper.php";

class TestHandler
{
    protected $TEST_HANDLER_SETTINGS;
    protected $TEST_HANDLER_SETTINGS_PATH;
    protected $BEHAT_SETTINGS_PATH;
    protected $BEHAT_FOLDER_PATH;
    protected $BEHAT_OUTPUT_FILE_PATH;

    public function __construct()
    {
        $this->BEHAT_SETTINGS_PATH = dirname(__FILE__) . "/../../behat.yml";
        $this->TEST_HANDLER_SETTINGS_PATH = dirname(__FILE__) . "/test-handler-settings.json";
        $this->BEHAT_FOLDER_PATH = dirname(__FILE__) . "/../../vendor/bin/behat";
        $this->BEHAT_OUTPUT_FILE_PATH = dirname(__FILE__) . "/output/" . TestHandlerHelper::getTimestamp() . '.html';
        $this->setTestHandlerSettings();
    }

    public function doBehatCmd(string $params = "")
    {
        // TODO: Refactoring -> put cli options in array
        $cmdResponse = TestHandlerHelper::executeShellCmd($this->BEHAT_FOLDER_PATH, array(
            "config" => $this->BEHAT_SETTINGS_PATH,
            "format" => "html",
            "out"    => $this->BEHAT_OUTPUT_FILE_PATH
        ));

        // TODO: Refactoring -> Create file and delete it right after is not a good practice
        $output = file_get_contents($this->BEHAT_OUTPUT_FILE_PATH);
        unlink($this->BEHAT_OUTPUT_FILE_PATH);

        return $output;
    }

    protected function setTestHandlerSettings()
    {
        $this->TEST_HANDLER_SETTINGS = TestHandlerHelper::getJsonFromFile($this->TEST_HANDLER_SETTINGS_PATH);
    }
}
