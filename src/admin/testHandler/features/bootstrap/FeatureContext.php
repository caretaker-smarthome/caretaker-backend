<?php

use Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

require_once dirname(__FILE__) . "/../../testHandlerHelper.php";


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends BehatContext
{
    protected $TEST_HANDLER_SETTINGS;
    protected $TEST_HANDLER_SETTINGS_PATH;
    protected $LINK_TO_API;


    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->TEST_HANDLER_SETTINGS_PATH = dirname(__FILE__) . "/../../test-handler-settings.json";
        $this->setTestHandlerSettings();
        $this->setLinkToApi();
    }

    protected function setApiSettings()
    { }

    protected function setTestHandlerSettings()
    {
        $this->TEST_HANDLER_SETTINGS = TestHandlerHelper::getJsonFromFile($this->TEST_HANDLER_SETTINGS_PATH);
    }

    protected function setLinkToApi()
    {
        $apiToTest = $this->TEST_HANDLER_SETTINGS["api_to_test"];

        if ($apiToTest === "local") {
            $this->LINK_TO_API = $this->TEST_HANDLER_SETTINGS["api"]["link_to_local_api"];
        } else {
            $this->LINK_TO_API = $this->TEST_HANDLER_SETTINGS["api"]["link_to_server_api"];
        }
    }

    /**
     * @Given the request body is :
     */
    public function theRequestBodyIs(PyStringNode $string)
    {
        $this->requestBody = $string;
    }

    /**
     * @Given /^I call "([^"]*)" cmd from API$/
     */
    public function iCallCmdFromApi(string $cmdName)
    {
        $this->requestResponse = TestHandlerHelper::doPostRequest($this->LINK_TO_API . "cmds/" . $cmdName . "/", json_decode($this->requestBody, TRUE));
    }

    /**
     * @Then /^I should get a response with success "([^"]*)"$/
     */
    public function iShouldGetAResponseWithSuccess(string $value)
    {
        $response =  json_decode($this->requestResponse["response"], TRUE);
        $responseSuccess = var_export($response["success"], TRUE);

        if ($responseSuccess !== $value) {
            throw new PendingException("Success of the request is not $value but $responseSuccess !");
        }
    }

    /**
     * @Then /^I should get a status "([^"]*)"$/
     */
    public function iShouldGetAStatus(string $status)
    {
        $requestStatus = TestHandlerHelper::getPostRequestStatus($this->requestResponse);

        if ($status !== strval($requestStatus)) {
            throw new PendingException("Status of the request is not $status but $requestStatus");
        }
    }
}
