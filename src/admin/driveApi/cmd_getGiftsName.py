from driveApiConnexion import DriveApi
from cliHandler import CliHandler


class GetGiftsName(DriveApi, CliHandler):

    def __init__(self):
        DriveApi.__init__(self)
        CliHandler.__init__(self)

    def getDataFromCol(self, sheet, col, rowToStart=0):
        fullCol = sheet.col_values(col)

        return fullCol[rowToStart:]

    def removeEmptyElmtInArray(self, arr):
        return ' '.join(arr).split()

    def getData(self):
        sheet = self.client.open("all_gifts").sheet1

        emailCell = sheet.find("gift_name")
        emailCellRow = emailCell.row
        emailCellCol = emailCell.col

        giftName = self.getDataFromCol(sheet, emailCellCol, emailCellRow)
        self.returnJson(self.removeEmptyElmtInArray(giftName))


GetGiftsName().getData()
