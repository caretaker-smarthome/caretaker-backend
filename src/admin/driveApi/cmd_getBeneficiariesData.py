from driveApiConnexion import DriveApi
from cliHandler import CliHandler


class GetBeneficiaryData(DriveApi, CliHandler):

    def __init__(self):
        DriveApi.__init__(self)
        CliHandler.__init__(self)

    def getDataFromCol(self, sheet, col, rowToStart=0):
        fullCol = sheet.col_values(col)

        return fullCol[rowToStart:]

    def getData(self):
        sheet = self.client.open("ayant_droit").sheet1

        emailCell = sheet.find("emails")
        emailCellRow = emailCell.row
        emailCellCol = emailCell.col

        self.returnJson(self.getDataFromCol(sheet, emailCellCol, emailCellRow))


GetBeneficiaryData().getData()
