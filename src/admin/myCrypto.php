<?php

require_once dirname(__FILE__) . "/../vendor/autoload.php";

use Defuse\Crypto\Crypto;

class MyCrypto
{
    protected $myCryptoSettings;

    public function __construct(array $settings)
    {
        $this->myCryptoSettings = $settings;
    }

    protected function getEncryptionKey()
    {
        $myfile = fopen($this->myCryptoSettings["key_to_encrypt_path"], "r");
        $pwd = fread($myfile, 100);
        fclose($myfile);

        return $pwd;
    }

    public function encrypt(string $msg)
    {
        return Crypto::encryptWithPassword($msg, self::getEncryptionKey());
    }

    public function decrypt(string $msg)
    {
        return Crypto::decryptWithPassword($msg, self::getEncryptionKey());
    }

    public function hashPassword(string $pwd)
    {
        return password_hash($pwd, PASSWORD_DEFAULT);
    }

    public function verifyPassword(string $pwd, string $hashedPwd)
    {
        return password_verify($pwd, $hashedPwd);
    }
}
