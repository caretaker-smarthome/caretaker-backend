<?php

require_once '../../request.php';

class SynchronizeBeneficiaries extends Request implements IRequest
{

	public function request()
	{
		$this->getRequestData();

		try {
			$this->synchronizeBeneficiariesRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function synchronizeBeneficiariesRequest()
	{
		$beneficiaries = $this->getPythonAPICmdResponse("getBeneficiariesData");

		$this->adventsKalenderDB->connect();

		$actualBeneficiariesEmail = $this->getActualBeneficiariesEmail();
		$beneficiariesToInsert = array_diff($beneficiaries, $actualBeneficiariesEmail);
		$beneficiariesToDelete = array_diff($actualBeneficiariesEmail, $beneficiaries);

		$this->insertNewBeneficiariesInDb($beneficiariesToInsert);
		$this->deleteBeneficiariesInDb($beneficiariesToDelete);

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"hasBeneficiariesBeenSynchronized" => TRUE,
				"actualBeneficiaries"              => $actualBeneficiariesEmail,
				"newBeneficiaries"                 => array_values($beneficiariesToInsert),
				"deletedBeneficiaries"			   => array_values($beneficiariesToDelete),
			)
		);
	}

	protected function insertNewBeneficiariesInDb(array $newBeneficiaries)
	{
		foreach ($newBeneficiaries as $beneficiary) {
			$this->adventsKalenderDB->query(
				'INSERT INTO beneficiaries(email) VALUES(:email)',
				array(
					'email' => $beneficiary,
				)
			);

			$this->sendEmailNotification($beneficiary);
		}
	}

	protected function sendEmailNotification(string $beneficiaryEmail)
	{
		$emailConstructor = array(
			"email"			  =>  array($beneficiaryEmail),
			"subject"		  => "Access to Account creation : ISFATES Adventskalender",
			"bodyConstructor" => array(
				"pathToTemplate" => "./accountCreationAccessTemplate.html",
				"replacer"		 => array(
					"__USER_NAME__" => $beneficiaryEmail,
				),
			)
		);

		$this->emailSender->sendEmail($emailConstructor);
	}

	protected function deleteBeneficiariesInDb(array $beneficiariesToDelete)
	{
		foreach ($beneficiariesToDelete as $beneficiaryToDelete) {
			$this->adventsKalenderDB->query(
				"DELETE FROM beneficiaries WHERE email= :email",
				array(
					"email" => $beneficiaryToDelete,
				)
			);
		}
	}

	protected function getActualBeneficiariesEmail()
	{
		$emails = array();
		foreach ($this->adventsKalenderDB->get("SELECT email FROM beneficiaries") as $beneficiary) {
			array_push($emails, $beneficiary["email"]);
		}

		return $emails;
	}
}


$synchronizeBeneficiariesClass = new SynchronizeBeneficiaries();
$synchronizeBeneficiariesClass->request();
