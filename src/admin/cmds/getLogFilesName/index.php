<?php

require '../../request.php';

class GetLogFilesName extends Request implements IRequest
{
	public function request()
	{
		try {
			$PATH_TO_LOGS = $this->API_SETTINGS["logs_handler"]["logs_folder_path"];
			$logFilesName = [];

			foreach (glob($PATH_TO_LOGS . "*") as $file) {
				array_push($logFilesName, str_replace($PATH_TO_LOGS, "", $file));
			}

			$this->returnSuccessResponse(
				array(
					'logFilesName' => $logFilesName,
				)
			);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}
}


$getLogFilesNameClass = new GetLogFilesName();
$getLogFilesNameClass->request();
