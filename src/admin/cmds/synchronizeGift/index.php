<?php

require '../../request.php';

class SynchronizeGift extends Request implements IRequest
{
	protected $validGiftsName = array("test");

	public function request()
	{
		$this->getRequestData();

		$giftsName = $this->userBody["giftsName"];

		try {
			$this->synchronizeGiftRequest($giftsName);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function isGiftNameValid(string $giftName)
	{
		return in_array($giftName, $this->validGiftsName);
	}

	protected function sendGiftDataToDb(string $giftName, $giftData)
	{
		if ($this->adventsKalenderDB->isPresent("SELECT id FROM gift WHERE name= :name", array(
			"name" => $giftName,
		))) {
			$this->adventsKalenderDB->query(
				"UPDATE gift SET id = :id, value = :value, text = :text, img = :img WHERE name = :name",
				array(
					"id"    => $giftData["id"],
					"name"  => $giftName,
					"value" => $giftData["value"],
					"text"  => json_encode($giftData["text"]),
					"img"   => json_encode($giftData["img"]),
				)
			);
		} else {
			$this->adventsKalenderDB->query(
				"INSERT INTO gift(id, name, value, text, img) VALUES(:id, :name, :value, :text, :img)",
				array(
					"id"    => $giftData["id"],
					"name"  => $giftName,
					"value" => $giftData["value"],
					"text"  => json_encode($giftData["text"]),
					"img"   => json_encode($giftData["img"]),
				)
			);
		}
	}

	protected function synchronizeGiftRequest(array $giftsName)
	{
		$giftsData = $this->getPythonAPICmdResponse("getGivenGiftsData", array(
			"giftsName" => $giftsName,
		));

		$this->adventsKalenderDB->connect();

		foreach ($giftsData as $giftName => $giftData) {
			$this->sendGiftDataToDb($giftName, $giftData);
		}

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"haveGiftsBeenSynchronized" => TRUE,
			)
		);
	}
}


$synchronizeGiftClass = new SynchronizeGift();
$synchronizeGiftClass->request();
