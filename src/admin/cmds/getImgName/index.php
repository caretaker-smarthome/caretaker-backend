<?php

require_once '../../request.php';

class GetImgName extends Request implements IRequest
{
    protected $imgId;

    public function request()
    {
        $this->getRequestData();
        $this->imgId = $this->userBody["id"];

        try {
            $this->getImgNameRequest();
        } catch (Exception $e) {
            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function getImgNameRequest()
    {
        $this->adventsKalenderDB->connect();

        $imgName = $this->getImgNameFromDb();

        $this->adventsKalenderDB->disconnect();

        $this->returnSuccessResponse(
            array(
                "name" => $imgName,
            )
        );
    }

    protected function getImgNameFromDb()
    {
        $getImgNameResponse = $this->adventsKalenderDB->get("SELECT hashed_name FROM image WHERE id = :imgId", array(
            "imgId" => $this->imgId,
        ));

        return $getImgNameResponse[0]["hashed_name"];
    }
}


$getImgNameClass = new GetImgName();
$getImgNameClass->request();
