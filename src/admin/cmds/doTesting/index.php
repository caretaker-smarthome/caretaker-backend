<?php

require '../../request.php';
require_once "../../../admin/testHandler/testHandler.php";

// TODO: Maybe rename to runTest ?
class DoTesting extends Request implements IRequest
{
	protected $generatedUserId;
	protected $userEmail;
	protected $testHandler;

	public function __construct()
	{
		$this->testHandler = new TestHandler();
	}

	public function request()
	{
		$this->getRequestData();

		try {
			$this->doTestingRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function doTestingRequest()
	{
		//$this->userEmail = $this->userBody["username"];

		$behatResponse = $this->testHandler->doBehatCmd();

		$this->returnSuccessResponse(
			array(
				"feedback" => $behatResponse,
			)
		);
	}
}


$doTestingClass = new DoTesting();
$doTestingClass->request();
