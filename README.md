# Caretaker-backend

----
### This project use:

* PHP :
    * [PHPMailer](https://github.com/PHPMailer/PHPMailer) to send email with PHP
    * [Monolog](https://github.com/Seldaek/monolog/blob/master/README.md) to manage logs
    * [Composer](https://getcomposer.org/) to manage PHP's package
    * [Behat **v2.5**](http://docs.behat.org/en/v2.5/) to test the API and the app

----
### Installation:

1. Install [Composer](https://getcomposer.org/)

2. Edit all `api-settings.json` with yours settings (there is one in `/api` and one in `/admin`)
    
    * `api-settings.json` structure in `/api`:
    ```json
    {
        "name": "Caretaker-backend",
        "version": "<version>",
        "devs_email": ["<emails>"],
        "api_to_use": "<local || server>",
        "ws_to_use": "<local || server>",
        "api": {
            "local": "<link/to/api>",
            "server": "https://api.caretaker-smarthome.eu/src/api/"
        },
        "ws": {
            "local": "<path/to/websockets/>",
            "server": "/wss/"
        },
        "gmail": {
            "gmail_api_credentials": "here/path/to/file"
        },
        "crypto": {
            "key_to_encrypt_path": "here/path/to/file"
        },
        "database": {
            "db_credentials_path": "here/path/to/file"
        },
        "logs_handler": {
            "logs_folder_path": "here/path/to/file"
        }
    }
    ```
    
    * `api-settings.json` structure in `/admin`:
    ```json
    {
        "name": "Caretaker-backend-admin",
        "version": "<version>",
         "gmail": {
            "gmail_api_credentials": "here/path/to/file"
        },
        "crypto": {
            "key_to_encrypt_path": "here/path/to/file"
        },
        "database": {
            "db_credentials_path": "here/path/to/file"
        },
        "logs_handler": {
            "logs_folder_path": "here/path/to/file"
        }
    }
    ```
    
    ##### ! You have to get all this files from an admin
    
3. Import database structure `/src/database.sql` to your local database

4. To run testing on local you will probably have to set the `curl.cainfo` in your `php.ini`. Check [here](https://stackoverflow.com/questions/29822686/curl-error-60-ssl-certificate-unable-to-get-local-issuer-certificate) for more infos